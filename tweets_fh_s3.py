'''
author: faraz mazhar
descri: Takes a stream of tweets and puts it into S3 using Firehose.
'''


from __future__ import absolute_import, print_function

import base64
import json
import sys

import boto3
from botocore.exceptions import ClientError
from tweepy import OAuthHandler, Stream
from tweepy.streaming import StreamListener


def get_secret():
    secret_name = "faraz/twitter-api-keys"
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name = 'secretsmanager',
        region_name = region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.
    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            unused_decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            
    # Your code goes here. 
    return secret

class FirehoseListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def __init__(self):
        self.client = boto3.client('firehose')
    
    def on_data(self, data):
        print(data)
        self.client.put_record(DeliveryStreamName = 'faraz-firehose',
                                Record = {
                                        'Data': data
                                        })
        return True

    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    secret_json = get_secret()
    secret = json.loads(secret_json)
    
    # Go to http://apps.twitter.com and create an app.
    # The consumer key and secret will be generated for you after
    consumer_key = secret['api_key']
    consumer_secret = secret['api_secret']
    
    # After the step above, you will be redirected to your app's page.
    # Create an access token under the the "Your access token" section
    access_token = secret['access_token']
    access_token_secret = secret['access_token_secret']
    
    l = FirehoseListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    stream = Stream(auth, l)
    stream.filter(track = sys.argv[1:], languages=['en'])
